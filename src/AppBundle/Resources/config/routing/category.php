<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('category', new Route('/', array(
    '_controller' => 'AppBundle:Category:index',
)));

$collection->add('category_show', new Route('/{id}/show', array(
    '_controller' => 'AppBundle:Category:show',
)));

$collection->add('category_new', new Route('/new', array(
    '_controller' => 'AppBundle:Category:new',
)));

$collection->add('category_create', new Route(
    '/create',
    array('_controller' => 'AppBundle:Category:create'),
    array(),
    array(),
    '',
    array(),
    'POST'
));

$collection->add('category_edit', new Route('/{id}/edit', array(
    '_controller' => 'AppBundle:Category:edit',
)));

$collection->add('category_update', new Route(
    '/{id}/update',
    array('_controller' => 'AppBundle:Category:update'),
    array(),
    array(),
    '',
    array(),
    array('POST', 'PUT')
));

$collection->add('category_delete', new Route(
    '/{id}/delete',
    array('_controller' => 'AppBundle:Category:delete'),
    array(),
    array(),
    '',
    array(),
    array('POST', 'DELETE')
));

return $collection;
