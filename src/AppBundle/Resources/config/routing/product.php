<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('product', new Route('/', array(
    '_controller' => 'AppBundle:Product:index',
)));

$collection->add('product_show', new Route('/{id}/show', array(
    '_controller' => 'AppBundle:Product:show',
)));

$collection->add('product_new', new Route('/new', array(
    '_controller' => 'AppBundle:Product:new',
)));

$collection->add('product_create', new Route(
    '/create',
    array('_controller' => 'AppBundle:Product:create'),
    array(),
    array(),
    '',
    array(),
    'POST'
));

$collection->add('product_edit', new Route('/{id}/edit', array(
    '_controller' => 'AppBundle:Product:edit',
)));

$collection->add('product_update', new Route(
    '/{id}/update',
    array('_controller' => 'AppBundle:Product:update'),
    array(),
    array(),
    '',
    array(),
    array('POST', 'PUT')
));

$collection->add('product_delete', new Route(
    '/{id}/delete',
    array('_controller' => 'AppBundle:Product:delete'),
    array(),
    array(),
    '',
    array(),
    array('POST', 'DELETE')
));

return $collection;
