<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_da4fd5c46b5c64934179316dacb79f3407e74fee1edf0e962aa5bff5b8d59d2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b26438b771f515fd8bdbe1c7346014a4bd974c1ef48238201a6f31b1ee91c67 = $this->env->getExtension("native_profiler");
        $__internal_8b26438b771f515fd8bdbe1c7346014a4bd974c1ef48238201a6f31b1ee91c67->enter($__internal_8b26438b771f515fd8bdbe1c7346014a4bd974c1ef48238201a6f31b1ee91c67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8b26438b771f515fd8bdbe1c7346014a4bd974c1ef48238201a6f31b1ee91c67->leave($__internal_8b26438b771f515fd8bdbe1c7346014a4bd974c1ef48238201a6f31b1ee91c67_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_576a6b8d0d291db3925d661a396aa7748b73ea3bc3d530abacc1c531a5cff2fc = $this->env->getExtension("native_profiler");
        $__internal_576a6b8d0d291db3925d661a396aa7748b73ea3bc3d530abacc1c531a5cff2fc->enter($__internal_576a6b8d0d291db3925d661a396aa7748b73ea3bc3d530abacc1c531a5cff2fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_576a6b8d0d291db3925d661a396aa7748b73ea3bc3d530abacc1c531a5cff2fc->leave($__internal_576a6b8d0d291db3925d661a396aa7748b73ea3bc3d530abacc1c531a5cff2fc_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ece194c260c06a4cc37c776b93a09ba7c325c3a0a604fbfbe116d0b3e76c01ba = $this->env->getExtension("native_profiler");
        $__internal_ece194c260c06a4cc37c776b93a09ba7c325c3a0a604fbfbe116d0b3e76c01ba->enter($__internal_ece194c260c06a4cc37c776b93a09ba7c325c3a0a604fbfbe116d0b3e76c01ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ece194c260c06a4cc37c776b93a09ba7c325c3a0a604fbfbe116d0b3e76c01ba->leave($__internal_ece194c260c06a4cc37c776b93a09ba7c325c3a0a604fbfbe116d0b3e76c01ba_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_61f476de5d5de5ba70b62b14c5b602c2f1ac21b31f1b19af790a6862e2e33b8e = $this->env->getExtension("native_profiler");
        $__internal_61f476de5d5de5ba70b62b14c5b602c2f1ac21b31f1b19af790a6862e2e33b8e->enter($__internal_61f476de5d5de5ba70b62b14c5b602c2f1ac21b31f1b19af790a6862e2e33b8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_61f476de5d5de5ba70b62b14c5b602c2f1ac21b31f1b19af790a6862e2e33b8e->leave($__internal_61f476de5d5de5ba70b62b14c5b602c2f1ac21b31f1b19af790a6862e2e33b8e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
