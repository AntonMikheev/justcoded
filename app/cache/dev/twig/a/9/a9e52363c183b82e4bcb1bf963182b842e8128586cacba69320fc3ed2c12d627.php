<?php

/* base.html.twig */
class __TwigTemplate_e4d83439f67d81e4f9a23c4f17393b2cbece3043223c95b7810d20e222ec1908 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc1f81586b9a774a8650a499b305c56a40db8c0dc057db73cfd4b2efbfcc0d14 = $this->env->getExtension("native_profiler");
        $__internal_dc1f81586b9a774a8650a499b305c56a40db8c0dc057db73cfd4b2efbfcc0d14->enter($__internal_dc1f81586b9a774a8650a499b305c56a40db8c0dc057db73cfd4b2efbfcc0d14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_dc1f81586b9a774a8650a499b305c56a40db8c0dc057db73cfd4b2efbfcc0d14->leave($__internal_dc1f81586b9a774a8650a499b305c56a40db8c0dc057db73cfd4b2efbfcc0d14_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c5f0243b74e64badfe86625bf4a2b44e4185877b1157540b06b404c0bbce1fec = $this->env->getExtension("native_profiler");
        $__internal_c5f0243b74e64badfe86625bf4a2b44e4185877b1157540b06b404c0bbce1fec->enter($__internal_c5f0243b74e64badfe86625bf4a2b44e4185877b1157540b06b404c0bbce1fec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_c5f0243b74e64badfe86625bf4a2b44e4185877b1157540b06b404c0bbce1fec->leave($__internal_c5f0243b74e64badfe86625bf4a2b44e4185877b1157540b06b404c0bbce1fec_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_54cf5de0d98a1e1e61325e3385c27462873a0fdf98eee029df7aaca320e85637 = $this->env->getExtension("native_profiler");
        $__internal_54cf5de0d98a1e1e61325e3385c27462873a0fdf98eee029df7aaca320e85637->enter($__internal_54cf5de0d98a1e1e61325e3385c27462873a0fdf98eee029df7aaca320e85637_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_54cf5de0d98a1e1e61325e3385c27462873a0fdf98eee029df7aaca320e85637->leave($__internal_54cf5de0d98a1e1e61325e3385c27462873a0fdf98eee029df7aaca320e85637_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_e6fce99cb410c413d94658d71325599ab7efa1907c61cf9fbf99a775d27044d8 = $this->env->getExtension("native_profiler");
        $__internal_e6fce99cb410c413d94658d71325599ab7efa1907c61cf9fbf99a775d27044d8->enter($__internal_e6fce99cb410c413d94658d71325599ab7efa1907c61cf9fbf99a775d27044d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_e6fce99cb410c413d94658d71325599ab7efa1907c61cf9fbf99a775d27044d8->leave($__internal_e6fce99cb410c413d94658d71325599ab7efa1907c61cf9fbf99a775d27044d8_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_631075940999bb00802c9f53bff4fca96e90e8b42fcc919c81b5af50fc6896d2 = $this->env->getExtension("native_profiler");
        $__internal_631075940999bb00802c9f53bff4fca96e90e8b42fcc919c81b5af50fc6896d2->enter($__internal_631075940999bb00802c9f53bff4fca96e90e8b42fcc919c81b5af50fc6896d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_631075940999bb00802c9f53bff4fca96e90e8b42fcc919c81b5af50fc6896d2->leave($__internal_631075940999bb00802c9f53bff4fca96e90e8b42fcc919c81b5af50fc6896d2_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
